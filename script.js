$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    };
    var gStt = 1;
    var gIdCourse = 0;
    var gCoursesCol = ["stt", "id", "courseCode", "courseName", "duration", "price", "discountPrice", "coverImage", "teacherName", "teacherPhoto", "level", "isPopular", "isTrending", "action"]
    var gTableData = $("#courses-table").DataTable({
        columns: [
            { data: gCoursesCol[0] },
            { data: gCoursesCol[1] },
            { data: gCoursesCol[2] },
            { data: gCoursesCol[3] },
            { data: gCoursesCol[4] },
            { data: gCoursesCol[5] },
            { data: gCoursesCol[6] },
            { data: gCoursesCol[7] },
            { data: gCoursesCol[8] },
            { data: gCoursesCol[9] },
            { data: gCoursesCol[10] },
            { data: gCoursesCol[11] },
            { data: gCoursesCol[12] },
            { data: gCoursesCol[13] },
        ],
        columnDefs: [
            {
                targets: 0,
                render: function () {
                    return gStt++
                }
            },
            {
                targets: 7,
                render: function (value) {
                    return `<img class="img-thumbnail" src="${value}">`
                }
            },
            {
                targets: 9,
                render: function (value) {
                    return `<img class="img-thumbnail" src="${value}">`
                }
            },
            {
                targets: 11,
                render: function (value) {
                    var checked = value ? 'checked' : '';
                    return "<input type='checkbox' " + checked + " >"
                }
            },
            {
                targets: 12,
                render: function (value) {
                    var checked = value ? 'checked' : '';
                    return "<input type='checkbox' " + checked + " >"
                }
            },
            {
                targets: 13,
                defaultContent: `
                    <button class="btn btn-link btn-update" data-toggle="tooltip" data-placement="bottom" title="Edit Course"><i class="fa-solid fa-pen-to-square text-primary pointer"></i></button>
                    <button class="btn btn-link btn-sm btn-delete" data-toggle="tooltip" data-placement="bottom" title="Delete Course"><i class="fa-solid fa-trash-can text-danger pointer"></i></button>
                `
            }
        ]
    });
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading()
    $("#btn-add").on("click", function () {
        onBtnAddCoursesClick();
    });
    $("#btn-create-add").on("click", function () {
        onBtnAcceptCreateClick();
    });
    $("#btn-create-cancel").on("click", function () {
        $("#create-courses-modal").modal("hide");
    });
    $("#courses-table").on("click", ".btn-update", function () {
        onBtnUpdateClick(this);
    });
    $("#btn-update-add").on("click", function () {
        onBtnAcceptUpdateClick();
    })
    $("#btn-update-cancel").on("click", function () {
        $("#update-courses-modal").modal("hide");
    });
    $("#courses-table").on("click", ".btn-delete", function () {
        onBtnDeleteClick(this);
    });
    $("#btn-accept-delete").on("click", function () {
        onBtnAcceptDeleteClick(gIdCourse);
    });
    $("#btn-cancel-delete").on("click", function () {
        $("#delete-course-modal").modal("hide");
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // hàm sử lý sự kiện tải lại trang
    function onPageLoading() {
        getPopularCoursesDB(); // sự kiện lấy ra khóa học Popular
        getTrendinggCoursesDB(); // sự kiện lấy ra khóa học TrenDing
        onLoadDataTable(gCoursesDB.courses); // sự kiện load dữ liệu vào bảng
    }
    // hàm xử lý sự kiện lấy ra khóa học Popular
    function getPopularCoursesDB() {
        var vPopularCourses = gCoursesDB.courses.filter(function (paramCourses) {
            return paramCourses.isPopular == true;
        });
        console.log(vPopularCourses);
        showCoursesPopular(vPopularCourses); // sự kiện hiển thị khóa học popular
    }
    // hàm xử lý sự kiện lấy ra khóa học TrenDing
    function getTrendinggCoursesDB() {
        var vTrendingCourses = gCoursesDB.courses.filter(function (paramCourses) {
            return paramCourses.isTrending == true;
        });
        console.log(vTrendingCourses);
        showCoursesTrending(vTrendingCourses); // sự kiện hiển thị khóa học trending
    }
    // hàm xử lý sự kiện hiển thị khóa học popular
    function showCoursesPopular(paramCoursesPopular) {
        $("#courses-popular").html("");
        for (var bI in paramCoursesPopular) {
            var htmls = `<div class="col-lg-3">
                <div class="card mb-4">
              <a href="" class="card-img-top">
                <img src="${paramCoursesPopular[bI].coverImage}" class="rounded-top card-img-top">
              </a>
              <div class="card-body">
                <h6>
                  <a href="">${paramCoursesPopular[bI].courseName}</a>
                </h6>
                <ul class="mb-3 list-inline">
                  <li class="list-inline-item">
                    <i class="far fa-clock mr-2"></i>
                    ${paramCoursesPopular[bI].duration}
                  </li>
                  <li class="list-inline-item">${paramCoursesPopular[bI].level}</li>
                </ul>
                <div class="mt-3">
                  <span class="text-dark font-weight-bold">$${paramCoursesPopular[bI].discountPrice}</span>
                  <del class="text-muted">$${paramCoursesPopular[bI].price}</del>
                </div>
              </div>
              <div class="card-footer">
                <div class="float-left mr-3">
                  <img src="${paramCoursesPopular[bI].teacherPhoto}" alt="" class="rounded-circle img-teacher">
                </div>
                <div class="float-left">
                  <small>${paramCoursesPopular[bI].teacherName}</small>
                </div>
                <div class="float-right">
                  <i class="far fa-bookmark text-secondary"></i>
                </div>
              </div>
            </div>
          </div>`;
            var vCoursesPopular = $("#courses-popular");
            vCoursesPopular.append(htmls);
        }
    }
    // hàm xử lý sự kiện hiển thị khóa học trending
    function showCoursesTrending(paramCoursesTrending) {
        $("#courses-trending").html("");
        for (var bI in paramCoursesTrending) {
            var htmls = `<div class="col-lg-3">
                <div class="card mb-4">
              <a href="" class="card-img-top">
                <img src="${paramCoursesTrending[bI].coverImage}" class="rounded-top card-img-top">
              </a>
              <div class="card-body">
                <h6>
                  <a href="">${paramCoursesTrending[bI].courseName}</a>
                </h6>
                <ul class="mb-3 list-inline">
                  <li class="list-inline-item">
                    <i class="far fa-clock mr-2"></i>
                    ${paramCoursesTrending[bI].duration}
                  </li>
                  <li class="list-inline-item">${paramCoursesTrending[bI].level}</li>
                </ul>
                <div class="mt-3">
                  <span class="text-dark font-weight-bold">${paramCoursesTrending[bI].discountPrice}</span>
                  <del class="text-muted">${paramCoursesTrending[bI].price}</del>
                </div>
              </div>
              <div class="card-footer">
                <div class="float-left mr-3">
                  <img src="${paramCoursesTrending[bI].teacherPhoto}" alt="" class="rounded-circle img-teacher">
                </div>
                <div class="float-left">
                  <small>${paramCoursesTrending[bI].teacherName}</small>
                </div>
                <div class="float-right">
                  <i class="far fa-bookmark text-secondary"></i>
                </div>
              </div>
            </div>
          </div>`;
            var vCoursesTreding = $("#courses-trending");
            vCoursesTreding.append(htmls);
        }
    }
    // hàm xử lý sự kiện load dữ liệu vào bảng
    function onLoadDataTable(paramCourses) {
        gStt = 1;
        var vTable = $("#courses-table").DataTable();
        vTable.clear();
        vTable.rows.add(paramCourses);
        vTable.draw();
    }
    // hàm xử lý sự kiện thêm khóa học mới
    function onBtnAddCoursesClick() {
        $("#create-courses-modal").modal();
    }
    // hàm xử lý sự kiện nút xác nhận thêm khóa học mới
    function onBtnAcceptCreateClick() {
        var vCourse = {
            id: 0,
            courseCode: "",
            courseName: "",
            price: -1,
            discountPrice: -1,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: "",
            isTrending: ""
        };
        // thu thập dữ liệu create
        readData(vCourse);
        // Kiểm tra dữ liệu create
        var isCheckCreate = checkCreate(vCourse);
        if (isCheckCreate) {
            // Hiển thị dữ liệu
            showData(vCourse);
        }
    }
    // hàm thu thập dữ liệu create
    function readData(paramCourse) {
        paramCourse.id = getNextId();
        paramCourse.courseCode = $("#inp-create-course-code").val().trim();
        paramCourse.courseName = $("#inp-create-course-name").val().trim();
        paramCourse.price = parseInt($("#inp-create-price").val().trim());
        paramCourse.discountPrice = parseInt($("#inp-create-price-discount").val().trim());
        paramCourse.duration = $("#inp-create-duration").val().trim();
        paramCourse.level = $("#select-level-create").val();
        paramCourse.coverImage = $("#inp-create-image").val().trim();
        paramCourse.teacherName = $("#select-teacher-create").val();
        paramCourse.teacherPhoto = $("#select-img-teacher-create").val();
        paramCourse.isPopular = $("input[name = 'popular']:checked").val() == 'true' ? true : false;
        paramCourse.isTrending = $("input[name = 'trending']:checked").val() == 'true' ? true : false;

    }
    // hàm Kiểm tra dữ liệu create
    function checkCreate(paramCourse) {
        if (paramCourse.courseCode == "" ||
            paramCourse.courseName == "" ||
            paramCourse.price == "" ||
            paramCourse.duration == "" ||
            paramCourse.level == "" ||
            paramCourse.teacherName == "" ||
            paramCourse.coverImage == "" ||
            paramCourse.teacherPhoto == ""

        ) {
            alert("Bạn cần nhập đủ các thông tin bắt buộc");
            return false;
        };
        if (paramCourse.isPopular == false && paramCourse.isTrending == false) {
            alert("Bạn cần phải chọn khóa học Popular hoặc Trending");
            return false;
        };
        if (isNaN(paramCourse.price)) {
            alert("Price cần phải nhập là số nguyên");
            return false;
        }
        if (paramCourse.price < 0) {
            alert("Số cần nhập phải lớn hơn 0");
            return false;
        }
        var vFilter = gCoursesDB.courses.find(function (paramCourseDB) {
            return paramCourseDB.courseCode == paramCourse.courseCode
        })
        if (vFilter) {
            alert("Course Code k được trùng");
            return false;
        }
        return true;
    }
    // hàm hiển thị dữ liệu
    function showData(paramCourse) {
        alert("Thêm Course thành công");
        console.log(paramCourse);
        gCoursesDB.courses.push(paramCourse);
        onLoadDataTable(gCoursesDB.courses);
        resetCreateForm(); // sự kiện reset form create modal
    }
    // hàm xử lý sự kiện reset form create modal
    function resetCreateForm() {
        $("#inp-create-course-code").val("");
        $("#inp-create-course-name").val("");
        $("#inp-create-price").val("");
        $("#inp-create-price-discount").val("");
        $("#inp-create-duration").val("");
        $("#select-level-create").val("");
        $("#inp-create-image").val("");
        $("#select-teacher-create").val("");
        $("#select-img-teacher-create").val("");
        $("input[name = 'trending']").prop("checked", "");
        $("input[name = 'popular']").prop("checked", "");
        $("#create-courses-modal").modal("hide");

    }
    // hàm sinh ra đc id tự tăng tiếp theo, dùng khi Thêm mới phần tử
    function getNextId() {
        var vNextId = 0;
        // Nếu mảng chưa có đối tượng nào thì Id = 1
        if (gCoursesDB.courses.length == 0) {
            vNextId = 1;
        }
        // Id tiếp theo bằng Id của phần tử cuối cùng + thêm 1    
        else {
            vNextId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1;
        }
        return vNextId;
    }
    // hàm xử lý sự kiện nút sửa
    function onBtnUpdateClick(paramButton) {
        $("#update-courses-modal").modal()
        var vTable = $("#courses-table").DataTable();
        var vRowClick = $(paramButton).closest("tr");
        var vDataRow = vTable.row(vRowClick).data();
        $("#inp-update-id").val(vDataRow.id)
        $("#inp-update-course-code").val(vDataRow.courseCode);
        $("#inp-update-course-name").val(vDataRow.courseName);
        $("#inp-update-price").val(vDataRow.price);
        $("#inp-update-price-discount").val(vDataRow.discountPrice);
        $("#inp-update-duration").val(vDataRow.duration);
        $("#select-level-update").val(vDataRow.level);
        $("#inp-update-image").val(vDataRow.coverImage);
        $("#select-teacher-update").val(vDataRow.teacherName);
        $("#select-img-teacher-update").val(vDataRow.teacherPhoto);
        $("#select-popular-update").val(vDataRow.isPopular == true ? "true" : "false");
        $("#select-trending-update").val(vDataRow.isTrending == true ? "true" : "false");
    }
    // hàm xử lý sự kiện xác nhận sửa
    function onBtnAcceptUpdateClick() {
        var vCourse = {
            id: 0,
            courseCode: "",
            courseName: "",
            price: -1,
            discountPrice: -1,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: "",
            isTrending: ""
        };
        // thu thập dữ liệu update
        readDataUpdate(vCourse);
        // Kiểm tra dữ liệu update
        var isCheckUpdate = checkUpdate(vCourse);
        if (isCheckUpdate) {
            // Hiển thị dữ liệu update
            showDataUpdate(vCourse);
        }
    }
    // hàm xử lý sự kiện thu thập dữ liệu update
    function readDataUpdate(paramCourse) {
        paramCourse.id = $("#inp-update-id").val();
        paramCourse.courseCode = $("#inp-update-course-code").val().trim();
        paramCourse.courseName = $("#inp-update-course-name").val().trim();
        paramCourse.price = parseInt($("#inp-update-price").val().trim());
        paramCourse.discountPrice = parseInt($("#inp-update-price-discount").val().trim());
        paramCourse.duration = $("#inp-update-duration").val().trim();
        paramCourse.level = $("#select-level-update").val();
        paramCourse.coverImage = $("#inp-update-image").val().trim();
        paramCourse.teacherName = $("#select-teacher-update").val();
        paramCourse.teacherPhoto = $("#select-img-teacher-update").val();
        paramCourse.isPopular = $("#select-popular-update").val() == 'true' ? true : false;
        paramCourse.isTrending = $("#select-trending-update").val() == 'true' ? true : false;
    }
    // hàm xử lý sự kiện Kiểm tra dữ liệu update
    function checkUpdate(paramCourse) {
        if (paramCourse.courseCode == "" ||
            paramCourse.courseName == "" ||
            paramCourse.price == "" ||
            paramCourse.duration == "" ||
            paramCourse.level == "" ||
            paramCourse.teacherName == "" ||
            paramCourse.coverImage == "" ||
            paramCourse.teacherPhoto == ""

        ) {
            alert("Bạn cần nhập đủ các thông tin bắt buộc");
            return false;
        };
        if (paramCourse.isPopular == false && paramCourse.isTrending == false) {
            alert("Bạn cần phải chọn khóa học Popular hoặc Trending");
            return false;
        };
        if (isNaN(paramCourse.price)) {
            alert("Price cần phải nhập là số nguyên");
            return false;
        }
        if (paramCourse.price < 0) {
            alert("Số cần nhập phải lớn hơn 0");
            return false;
        }
        return true;
    }
    // hàm xử lý hiển thị dữ liệu update
    function showDataUpdate(paramCourse) {
        var bIdIndex = gCoursesDB.courses.findIndex(function (paramCourseDB) {
            return paramCourseDB.id == paramCourse.id;
        });
        delete gCoursesDB.courses[bIdIndex];
        gCoursesDB.courses[bIdIndex] = paramCourse;
        onLoadDataTable(gCoursesDB.courses);
        $("#update-courses-modal").modal("hide");
        resetUpdateForm();
    }
    // hàm xử lý xự kiện reset form modal update
    function resetUpdateForm() {
        $("#inp-update-id").val("")
        $("#inp-update-course-code").val("");
        $("#inp-update-course-name").val("");
        $("#inp-update-price").val("");
        $("#inp-update-price-discount").val("");
        $("#inp-update-duration").val("");
        $("#select-level-update").val("");
        $("#inp-update-image").val("");
        $("#select-teacher-update").val("");
        $("#select-img-teacher-update").val("");
        $("#select-popular-update").val("");
        $("#select-trending-update").val("");
    }
    // hàm xử lý sự kiện nút xóa được ấn
    function onBtnDeleteClick(paramButton) {
        $("#delete-course-modal").modal();
        var vTable = $("#courses-table").DataTable();
        var vRowClick = $(paramButton).closest("tr");
        var vDataRow = vTable.row(vRowClick).data();
        gIdCourse = vDataRow.id;
        $("#inp-delete-course-code").val(vDataRow.courseCode)
    }
    // hàm xử lý sự kiện xác nhận xóa khóa học
    function onBtnAcceptDeleteClick(paramId) {
        var bIdIndex = gCoursesDB.courses.findIndex(function (paramCourseDB) {
            return paramCourseDB.id == paramId;
        });
        gCoursesDB.courses.splice(bIdIndex, 1);
        gIdCourse = 0;
        onLoadDataTable(gCoursesDB.courses);
        $("#delete-course-modal").modal("hide");
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
});









